import csv
import datetime
from collections import Counter

date='1/16/20'
starttime='00:00:00'
endtime='23:59:59'

uids = []

with open('jan29activity.csv') as mydata:
	data = csv.reader(mydata)
	for row in data:
		#if datetime.datetime.strptime(row[0], '%m/%d/%y') == datetime.datetime.strptime(date, '%m/%d/%y') and
		if datetime.datetime.strptime(row[1][:8],'%H:%M:%S') > datetime.datetime.strptime(starttime, '%H:%M:%S') and datetime.datetime.strptime(row[1][:8], '%H:%M:%S') < datetime.datetime.strptime(endtime, '%H:%M:%S'):
			uids.append(row[2])
			print(row[2])
		else:
			print(row,' out of date range')

print(Counter(uids).most_common(25))