#!/bin/bash

d=$(date +%FT%H.%M.%S)
filename=nightlyRestartLog.${d}.txt

for server in ce15 9acb # mupt
do
	case $server in
		ce15)
			ip=10.142.0.8 ;;
		9acb)
			ip=10.142.0.6 ;;
	#	mupt)
	#		ip=10.142.0.16 ;; mupt is not in rotation
	esac

	ssh $server sudo service nginx stop
	sleep 4
			# add condition for if tomcat is already down? will issue startp after 60 seconds anyway
	ssh $server sudo su fruser /opt/forgerock/apache-tomcat-8.5.32/bin/shutdown.sh &>> $filename

	processes=2
	seconds=0

	while [[ "$processes" -ne '0' ]];
	do
		sleep 1
		seconds=$((seconds + 1))
		processes=$(ssh $server ps -ef | grep tomcat | wc -l)
		echo seconds: $seconds, tomcat processess running on $server: $processes >> $filename

	if [[ "$seconds" -gt '60' ]];
	then
		echo 60 second limit reached on $server, restarting... >> $filename
		break
	fi
	done

	sleep 4

	ssh $server sudo su fruser /opt/forgerock/apache-tomcat-8.5.32/bin/startup.sh >> $filename

	sleep 4

	ssh $server sudo service nginx start

	sleep 4
done